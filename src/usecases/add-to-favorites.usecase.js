import { PokemonList } from "../repository/favorites";
export class AddToFavorites {

    static execute(pokemonSelected) {
        const favorites = new PokemonList()
        return favorites.add(pokemonSelected)
    }
}
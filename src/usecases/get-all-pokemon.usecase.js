import { Repository } from "../repository/repository";

export class GetAllPokemon {

    static async execute() {
        const repository = new Repository();
        const pokemons = await repository.getAllPokemon();
        const result = pokemons.results.map(pokemon => pokemon.name)

        return result
    }
}
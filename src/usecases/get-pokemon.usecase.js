import { Pokemon } from "../model/pokemon";
import { Repository } from "../repository/repository";

export class GetPokemon {
    static async execute(pokemon) {
        const repository = new Repository();
        try{
            const pokemonData = await repository.getPokemon(pokemon);
            return new Pokemon({
                id: pokemonData.id,
                name: pokemonData.name,
                image: pokemonData.sprites.other.dream_world.front_default,
                types: pokemonData.types.map( type => type.type.name )
            })
        } catch(error) {

            return new Pokemon({
                id: '',
                name: 'Not Found',
                image: '',
                types: ''
            })
        }
    }
}
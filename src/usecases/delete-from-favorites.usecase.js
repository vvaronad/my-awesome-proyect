import { PokemonList } from "../repository/favorites";

export class RemoveFromFavorites {

    static execute(selectedPokemon) {
        const favorites = new PokemonList()
        return favorites.delete(selectedPokemon)
    }
}
import { LitElement, html, css } from "lit";
import { PokemonList } from "../repository/favorites";

export class CardComponent extends LitElement{

    constructor() {
        super();
        this.pokemon = '';
    }

    connectedCallback(){
        super.connectedCallback()
    }
    
    static get properties() {
        return {
            pokemon: { type: String },
        };
    }

    handleFav(event) {
        event.preventDefault();
        const click = new CustomEvent("fav:click", {
            bubbles: true,
            composed: true,
            detail: {
                pokemon: this.pokemon,
            },
        });
        this.requestUpdate();
        this.dispatchEvent(click);
    }
    
    handleClick(event) {
        event.preventDefault();
        const click = new CustomEvent("element:click", {
            bubbles: true,
            composed: true,
            detail: {
                pokemon: event.target.innerText,
          },
        });
        this.dispatchEvent(click);
      }

    
    static styles = css`

    .cards__element {
        list-style: none;
        text-transform: capitalize;
        text-align: center;
        background-color: lavenderblush;
        border-radius: 7px;
        font-size: 1.1rem;
        margin: -0.1rem .75rem;
        transition: ease 200ms;
        position: relative;
    }
    .cards__element:hover {
        background-color: white;
    }

    p {
        padding: 1.5rem;
    }

    button {
        font-size: 20px;
        background-color: transparent;
        position: absolute;
        bottom: 40%;
        right: 10%;
        border: none;
        transition: ease-out 150ms
    } 
    
    button:active{
        scale: .9;
        padding: .25rem;
        scale: 1.1
    }
    `;


    render(){
        const isFavoriteIcon = PokemonList.Favorites.has(this.pokemon) ? '🧡' : '🤍'
        return html`
            <li class="cards__element">
                <button @click=${this.handleFav}>
                    ${isFavoriteIcon}
                </button>
                <p @click=${this.handleClick}>${this.pokemon}</p>
            </li>
            `
    }

}

customElements.define("card-element", CardComponent)
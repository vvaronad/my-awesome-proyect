import { LitElement, html, css } from "lit";
import './card.component'

class ListComponent extends LitElement {
  constructor() {
    super();
    this.pokemons = [];
  }

  static get properties() {
    return {
      pokemons: { type: Array, state: true },
    };
  }

  connectedCallback() {
    super.connectedCallback();
  }

  createRenderRoot() {
    return this;
  }

  render() {
    return html`
      <ul class="cards__list">
        ${this.pokemons?.map((pokemon) => {
          return html`
          <card-element pokemon=${pokemon}></card-element>`;
        })}
      </ul>
    `;
  }
}

// eslint-disable-next-line no-undef
customElements.define("list-component", ListComponent);

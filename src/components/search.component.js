import { LitElement, html } from "lit";

export class SearchComponent extends LitElement {
  constructor() {
    super();
    this.inputValue = "";
  }

  createRenderRoot() {
    return this;
  }

  handleInput(event) {
    this.inputValue = event.target.value;
  }

  onClick(event) {
    event.preventDefault();
    const click = new CustomEvent("search:submit", {
      bubbles: true,
      composed: true,
      detail: {
          search: this.inputValue, 
      },
    })
    this.dispatchEvent(click);
  }

  render() {
    return html`
      <form class="search__section">
        <input class="search__text-input" type="search" title="Search" @input=${this.handleInput} />
        <input class="search__submit-input" type="submit" value="search" @click=${this.onClick} />
      </form>
    `;
  }
}

// eslint-disable-next-line no-undef
customElements.define("search-component", SearchComponent);

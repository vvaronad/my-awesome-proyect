import { LitElement, html } from "lit";

class DetailComponent extends LitElement {
  constructor() {
    super();
    this.pokemon = {};
  }

  static get properties() {
    return {
      pokemon: { type: Object, state: true },
    };
  }

  connectedCallback() {
    super.connectedCallback();
  }

  createRenderRoot() {
    return this;
  }

  render() {
    return html`
  
      <section class="detail__section">
          <h2>${this.pokemon.name}</h2>
          <img src=${this.pokemon.image}/>
          <p>${this.pokemon.types}</p>
      </section>
    
    `;
  }
}

// eslint-disable-next-line no-undef
customElements.define("detail-component", DetailComponent);

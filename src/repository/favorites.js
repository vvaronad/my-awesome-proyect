export class PokemonList {
    static Favorites = new Set();

    static add(selectedPokemon){ 
        this.Favorites.add(selectedPokemon)
        return this.Favorites 
    }

    static delete(selectedPokemon){ 
        this.Favorites.delete(selectedPokemon)
        return this.Favorites
    }
}


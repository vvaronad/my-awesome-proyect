import axios from 'axios';

export class Repository {

    async getAllPokemon() {
        const pokemon = await (await axios.get(`https://pokeapi.co/api/v2/pokemon/?offset=0&limit=1281`)).data
        return pokemon
    }

    async getPokemon(name) {
        const pokemon = await (await axios.get(`https://pokeapi.co/api/v2/pokemon/${name}`)).data
        return pokemon
    }
}
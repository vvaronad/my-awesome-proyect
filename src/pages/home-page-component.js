import '../main.css';
import { LitElement, html } from 'lit';
import { GetAllPokemon } from '../usecases/get-all-pokemon.usecase';
import { GetPokemon } from '../usecases/get-pokemon.usecase';

import '../components/search.component'
import '../components/list.component'
import '../components/detail.component'
import { PokemonList } from '../repository/favorites';


class HomepageComponent extends LitElement {
    constructor() {
        super();
        this.allPokemons = [];
        this.selectedPokemon = {}
        this.favorites
    }

    async connectedCallback(){
        super.connectedCallback()

        
        this.addEventListener('fav:click', (e) =>{
            const selected = e.detail.pokemon
            PokemonList.Favorites.has(selected) 
            ? PokemonList.delete(selected) 
            : PokemonList.add(selected) 
        })
        
        this.addEventListener('search:submit', async (e) =>{
            const search = e.detail.search;
            const result = await GetPokemon.execute(search.toLocaleLowerCase())
            this.selectedPokemon = {...result}
            this.requestUpdate()
        })
        
        this.addEventListener('element:click', async (e) =>{
            const search = e.detail.pokemon
            const result = await GetPokemon.execute(search.toLocaleLowerCase())
            this.selectedPokemon = {...result}
            this.requestUpdate()
        })
        
        this.allPokemons = await GetAllPokemon.execute()
        this.requestUpdate()
    }

    createRenderRoot() {
        return this;
    }

    render(){
        return html`
        <section class="home__section">
            <search-component  class="detail__container"></search-component>
            <section class="home__content">
                <list-component .pokemons=${this.allPokemons}></list-component>
                <detail-component .pokemon=${this.selectedPokemon}></detail-component>
            </section>
        </section>
        `
    }
}
customElements.define("home-page-component", HomepageComponent);
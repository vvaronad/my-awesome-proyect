import '../main.css';
import { LitElement, html } from 'lit';

import '../components/search.component'
import '../components/list.component'
import '../components/detail.component'
import { PokemonList } from '../repository/favorites';


export class FavoritesPageComponent extends LitElement {
    constructor() {
        super();
        this.favoritePokemons = [...PokemonList.Favorites]
    }

    connectedCallback(){
        super.connectedCallback()
    }

    createRenderRoot() {
        return this;
    }

    render() {
        return html` 
        <ul class="cards__list">
            ${this.favoritePokemons?.map((pokemon) => {
                return html`
                <card-element pokemon=${pokemon}></card-element>`;
              })}
        </ul>
    
        `
    }
}
customElements.define("favorites-page-component", FavoritesPageComponent);
import '../pages/home-page-component'
export class HomePage extends HTMLElement {
    createRenderRoot(){
        return this;
    }
    connectedCallback() {
        this.innerHTML = `<home-page-component></home-page-component>`;
    }
}

customElements.define("home-page", HomePage)
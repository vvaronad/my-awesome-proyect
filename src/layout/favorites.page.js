import '../pages/favorites-page-component'

export class FavoritesPage extends HTMLElement {
    createRenderRoot(){
        return this;
    }
    connectedCallback() {
        this.innerHTML = `<favorites-page-component></favorites-page-component>`;
    }
}

customElements.define("favorites-page", FavoritesPage)
export class Pokemon {
    constructor({ id, name, image, types}) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.types = types;
    }
}
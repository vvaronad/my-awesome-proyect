import './main.css';
import { LitElement, html } from 'lit';
import { Router } from '@vaadin/router'

import '../src/layout/home.page'
import '../src/layout/favorites.page'

const outlet = document.querySelector('#outlet')
const router = new Router(outlet)
router.setRoutes([
    { path: '/', component: 'home-page'},
    { path: '/favorites', component: 'favorites-page'},
    { path: '(.*)', redirect: '/'},
])

class AppElement extends LitElement {

    createRenderRoot() {
        return this;
    }

    render(){
        return html`
        <home-page></home-page>
        <favorites-page></favorites-page>
        `
    }
}
customElements.define("app-element", AppElement);
Cypress.on('uncaught:exception', (err, runnable) => {
  return false;
});

describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080/')
    cy.get('.search__text-input').type('pikachu')
    cy.get('.search__submit-input').click()
    cy.get('h2').should('exist')
    cy.get('[pokemon="ivysaur"]').click()
    cy.get('[pokemon="ivysaur"]').shadow().find('button').click()
    cy.get('[href="/favorites"]').click()
    cy.get('card-element').should('exist')
    cy.get('.header__nav > [href="/"]').click()
    cy.get('[pokemon="ivysaur"]').shadow().find('button').click()
    cy.get('[href="/favorites"]').click()
    cy.get('card-element').should('not.exist')
    cy.get('h1').click()
  })
})
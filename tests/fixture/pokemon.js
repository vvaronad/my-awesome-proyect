export const dummyPokemonData = {
    id: 25,
    name: 'pikachu',
    sprites: {
        other: {
            dream_world: {
                front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png',
            }
        }
    },
    types: [
        { type: { name: 'electric' } },
    ]
}
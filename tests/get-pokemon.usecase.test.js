import { Repository } from "../src/repository/repository";
import { GetPokemon } from "../src/usecases/get-pokemon.usecase"
import { dummyPokemonData } from "./fixture/pokemon";

// eslint-disable-next-line no-undef
jest.mock('../src/repository/repository')

describe('get pokemon use case', () => {

    beforeEach(() => {
        Repository.mockClear();
    });

    it('should get a pokemon info', async () => {
        const expectedPokemonData = {
            id: dummyPokemonData.id,
            name: dummyPokemonData.name,
            image: dummyPokemonData.sprites.other.dream_world.front_default,
            types: dummyPokemonData.types.map( type => type.type.name)
        }

        Repository.mockImplementation(() => {
            return {
                getPokemon: () => {
                    return dummyPokemonData;
                }
            };
        });

        const pokemon = 'pikachu'
        const pokemonData = await GetPokemon.execute(pokemon);

        expect(pokemonData).toEqual(expectedPokemonData)
    })
})
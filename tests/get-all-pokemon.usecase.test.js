import { describe } from "node:test";
import { GetAllPokemon } from "../src/usecases/get-all-pokemon.usecase";
import { Repository } from "../src/repository/repository";
import { dummyPokemonsData } from "./fixture/pokemons";

// eslint-disable-next-line no-undef
jest.mock('../src/repository/repository')

describe("get all pokemon", () => {
    
    beforeEach(() => {
        Repository.mockClear();
    });

    it('should get a list of pokemons', async () => {
    
        const expectedPokemons = [
            'bulbasaur',  'ivysaur',
            'venusaur',   'charmander',
            'charmeleon', 'charizard',
            'squirtle',   'wartortle',
            'blastoise',  'caterpie',
            'metapod',    'butterfree',
            'weedle',     'kakuna',
            'beedrill',   'pidgey',
            'pidgeotto',  'pidgeot',
            'rattata',    'raticate'
          ]

        Repository.mockImplementation(() => {
            return {
                getAllPokemon: () => {
                    return dummyPokemonsData
                }
            };
        });

        const pokemons = await GetAllPokemon.execute()

        expect(pokemons).toBeTruthy()
        expect(pokemons).toEqual(expectedPokemons)
    })
})
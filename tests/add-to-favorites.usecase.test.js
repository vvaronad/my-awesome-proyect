import { describe } from "node:test";
import { AddToFavorites } from "../src/usecases/add-to-favorites.usecase";
import { PokemonList } from "../src/repository/favorites";

jest.mock("../src/repository/favorites")

describe("add pokemon to favorites", () => {

    beforeEach(() => {
        PokemonList.mockClear();
    });
    
    it("should add a pokemon to the favorites list", () => {
        const pokemon = 'kakuna'
        const mockReturnValue = 'kakuna';
        const addMock = jest.fn().mockImplementation(() => mockReturnValue);
        
        PokemonList.mockImplementation(() => ({
            add: addMock
        })) 
        
        const result = AddToFavorites.execute(pokemon)

        expect(addMock).toHaveBeenCalledWith(pokemon);
        expect(result).toBe(pokemon)
    })
})
import { describe } from "node:test";
import { RemoveFromFavorites } from "../src/usecases/delete-from-favorites.usecase";
import { PokemonList } from "../src/repository/favorites";

jest.mock("../src/repository/favorites")

describe("remove pokemon from favorites", () => {

    beforeEach(() => {
        PokemonList.mockClear();
    });

    it("should remove a pokemon from the favorites list", () => {
        const pokemon = 'kakuna';
        const mockReturnValue = 'pikachu';
        const deleteMock = jest.fn().mockImplementation(() => mockReturnValue);

        PokemonList.mockImplementation(() => ({
            delete: deleteMock
        }))
        const result = RemoveFromFavorites.execute(pokemon);

        expect(deleteMock).toHaveBeenCalledWith(pokemon);
        expect(result).toBe(mockReturnValue);

    })

})